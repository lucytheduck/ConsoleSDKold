/**
 /* This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package beer;

import java.util.*;

import org.lwjgl.examples.spaceinvaders.*;
import org.lwjgl.input.Keyboard;
import vodka.*;
import vodka.Sprite;
import vodka.Texture;

import java.io.Serializable;
import java.util.Timer;

/**
 * Created by lucy on 23.03.17.
 */
public class Game implements Serializable {
    boolean newX, newY;
    public boolean isRunning = true;
    Animation xa, ya;
    Sprite sprite;
    double xdir=0.1, ydir=0.1;
    public void init(){
        //is called when the game starts

        //this is a short example
        sprite = new Sprite();
        int[] tex = new int[1];
        tex[0] = Texture.loadTexture("test.png");
        sprite.setTexture_id(tex);
        sprite.setScale(new Point(1.0, 1.0));
        sprite.enabled = true;

        ya = new Animation(sprite){
            double y=0;
            @Override
            public void step() {
                super.step();
                sprite.position.setY(y+=ydir);
                if(y>1 || y<-1) {
                    ydir *= -1;
                    if (newY) ydir = random.nextDouble() / 50;
                }
            }
        };
        xa = new Animation(sprite){
            double x=0;
            @Override
            public void step() {
                super.step();
                sprite.position.setX(x+=xdir);
                if(x>1 || x<-1) {
                    xdir *= -1;
                    if(newX) xdir = random.nextDouble() / 50;
                }
            }
        };
        xa.enable();
        ya.enable();
    }

    Random random = new Random();
    public void step(){
        try {Thread.sleep(1000);}catch (InterruptedException e){e.printStackTrace();}
        newX = newY = true;
        //is called frequently while the game is running
    }
    public void pause(){
        //is called when the game pauses
    }
    public void resume(){
        //is called when the game resumes
    }
    public void exit(int code){
        //keep this line because I was lazy
        isRunning = false;
    }
}
