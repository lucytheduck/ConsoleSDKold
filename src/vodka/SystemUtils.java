/**
 /* This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package vodka;

import beer.Game;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucy on 11.05.2017.
 */
public class SystemUtils {
    private static KeyPair key;
    private static Cipher cipher;
    private static boolean init = false;
    public static void init(){
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            TimeKeeper.mark("initKeyPair");
            System.out.println("keySize = "+(1024*4));
            keyGen.initialize(1024*4);//16384
            key = keyGen.generateKeyPair();
            cipher = Cipher.getInstance("RSA");
            TimeKeeper.print("initKeyPair");
            init = true;
        } catch (Exception e2){
            e2.printStackTrace();
        }

    }
    private static String decrypt(String msg){
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key.getPrivate());
            return Base64.encodeBase64String(cipher.doFinal(msg.getBytes("UTF-8")));
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    private static String encrypt(String msg){
        try {
            cipher.init(Cipher.DECRYPT_MODE, key.getPublic());
            return new String(cipher.doFinal(Base64.decodeBase64(msg)), "UTF-8");
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public static ArrayList<String> getGamesList(){
        String[] s = new File("games").list();
        ArrayList<String> r = new ArrayList<>();
        for(String n : s)r.add(n);
        return r;
    }
    public String getIcon(String name){
        try {
            List<String> cm = Files.readAllLines(Paths.get("games/" + name + "/config.txt"));
            for (String line : cm)
                if(line.startsWith("icon "))return "games"+name+"/"+line.split(" ")[1];
        } catch (IOException e){
            e.printStackTrace();
        }
        return "junk/noicon.png";
    }
    public static void loadGame(String name){
        if(GameRuntime.debug)System.out.println("load - not available in debug mode");
        try {
            Game game = null;
            List<String> cm = Files.readAllLines(Paths.get("games/"+name+"/config.txt"));
            for(String line : cm){
                FileInputStream in = new FileInputStream("games/"+name+"/"+line.split(" ")[1]);
                System.out.println("games/"+name+"/"+line.split(" ")[1]);
            }
            //clear active dir
            for(File f : new File("active/beer").listFiles())
                f.delete();
            //copy game files
            for(File f : new File("games/"+name).listFiles()){
                Files.copy(f.toPath(),
                        new File("active/beer/"+f.getName()).toPath(),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("not installed (io)");
        }
    }
}