package vodka;

/**
 * Created by lucy on 01.07.17.
 */
public class GameThread extends Thread{
    public GameThread(Runnable runnable){
        super(runnable);
        Trashman.threads.add(this);
    }
    public void disable(){
        enabled = false;
    }
    private boolean enabled = true;
}
