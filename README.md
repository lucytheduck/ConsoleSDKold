# ConsoleSDK :duck: :duck: :duck:
School project - feel free to use it

ConsoleSDK Copyright © 2017 javalucy

# you'll need:
* [lwjgl (the old 2.9.3)](http://legacy.lwjgl.org/download.php.html)
* [JLayer 1.0.1](http://www.javazoom.net/javalayer/javalayer.html)
* [commons-codec-1.10](https://commons.apache.org/proper/commons-codec/)

# contact:
* [fediverse](lucy@kawen.space)
* [email](mailto:trashlucy@ducksareawesome.net)
  
#
This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3.0, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
